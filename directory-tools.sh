#!/bin/bash

# Directory Tools Script
# (Add symlink in /etc/cron.daily)
# Version : 1.0.2-5
# Release : 2021-06-24
# Author  : Artur Meinild

# Initial configuration variables
scriptpath="/path/to/script"

# Include common Bash functions
# shellcheck source=/dev/null
source "${scriptpath}/bash-common.sh"

# Include configuration file
if [[ -f "${scriptpath}/$1" ]]
then
  configfile="${scriptpath}/$1"
else
  configfile="${scriptpath}/directory-tools.conf"
fi
# shellcheck source=/path/to/script/directory-tools.conf
source "$configfile"

# Variable overrides
#roppaths=("/home/${user}/test")
#sympaths=("/home/${user}/test")
#tmppaths=("/home/${user}/test")

# Local function definition
create_log() {
  if [[ -d "${logfile%/*}" ]]
  then
    [[ -e "${logfile}" ]] && rm "${logfile}"
  else
    logfile="/dev/null"
  fi
}

print_status() {
  # Add pluralis (dirs/links/files)
  if [[ $count -eq 1 ]]
  then
    plu=""
  else
    plu="s"
  fi
  # Print success message if operation is successful ( count > 0 ),
  # or failure message when count = 0, then reset counter
  if [[ $count -gt 0 ]]
  then
    echo -e -n " \e[92m\u2713\e[39m  "
    echo -e -n " \u2713  " >> "$logfile"
  elif [[ -n $count ]]
  then
    echo -e -n " \e[91m\u2717\e[39m  "
    echo -e -n " \u2717  " >> "$logfile"
  fi
  # Print different messages according to mode
  case "${mode}" in
  "Opchange")
    printf "%-21s" "$count dir${plu} targeted" | tee -a "$logfile"
    ;;
  "Symtouch")
    printf "%-21s" "$count link${plu} touched" | tee -a "$logfile"
    ;;
  "Tmpclean")
    printf "%-21s" "$count file${plu} cleaned" | tee -a "$logfile"
    ;;
  esac
  # Print current operating path
  echo -e "Location \u2771 ${path}" | tee -a "$logfile"
}


create_log

script_start >> "$logfile" &
script_start


# Change owner and permissions in "${roppaths[@]}"
# shellcheck disable=SC2128
if [[ -n "$roppaths" ]]
then
  mode="Opchange"
  echo " <> Owner/permissions as stated in: $configfile <>" | tee -a "$logfile"

  # Read array and change owner/permissions
  for cmbpath in "${roppaths[@]}"
  do
    count=0

    path="$(echo "$cmbpath" | cut -d ' ' -f1)"
    ownr="$(echo "$cmbpath" | cut -d ' ' -f2)"
    perm="$(echo "$cmbpath" | cut -d ' ' -f3)"

    count=$(find "$path" -type d | wc -l)

    chown -R "$ownr" "$path"
    chmod -R "$perm" "$path"
    chmod -R +X "$path"
    [[ "${perm: -1}" -eq 0 ]] && chmod -R o-x "$path"

    if [[ "${verbosity,,}" == "yes" ]]
    then
      echo "chown -R $ownr $path"
      echo "chmod -R $perm $path"
      echo "chmod -R +X $path"
      [[ "${perm: -1}" -eq 0 ]] && echo "chmod -R o-x $path"
    fi

  print_status
  done
fi


# Touch symlinks in "${sympaths[@]}"
# shellcheck disable=SC2128
if [[ -n "$sympaths" ]]
then
  mode="Symtouch"
  echo " <> Touching symlinks as stated in: $configfile <>" | tee -a "$logfile"

  # Create named pipe
  [[ ! -d  ~/pipe ]] && mkdir ~/pipe
  [[ ! -e  ~/pipe/sym-list ]] && mkfifo ~/pipe/sym-list

  for path in "${sympaths[@]}"
  do
    # List symlinks in dir (recursively) and write them to pipe
    touch ~/pipe/sym-list
    # Add '-maxdepth X' to limit recurse depth
    find "$path" -type l > ~/pipe/sym-list &
    count=0

    # Read pipe and touch symlinks
    while read -r line
    do
      if [[ -n "$line" ]]
      then
        touch -h --reference="$(readlink -f "$line")" "$line"
        # shellcheck disable=SC2086,SC2181
        [[ $? -eq 0 ]] && [[ "${verbosity,,}" == "yes" ]] && echo "touch -h --reference=$(readlink -f $line) $line"
        (( count++ ))
      fi
    done < ~/pipe/sym-list

  print_status
  done
fi


# Clean out .bak / .swp / .tmp in "${tmppaths[@]}"
# shellcheck disable=SC2128
if [[ -n "$tmppaths" ]]
then
  mode="Tmpclean"
  echo " <> Cleaning tmpfiles as stated in: $configfile <>" | tee -a "$logfile"

  # Create named pipe
  [[ ! -d  ~/pipe ]] && mkdir ~/pipe
  [[ ! -e  ~/pipe/tmp-list ]] && mkfifo ~/pipe/tmp-list

  for path in "${tmppaths[@]}"
  do
    # List .bak / .swp / .tmp files in dir (recursively) and write them to pipe
    touch ~/pipe/tmp-list
    # Add '-maxdepth X' to limit recurse depth
    find "$path" -name "*.bak*" -o -name "*.swp*" -o -name "*.tmp*" > ~/pipe/tmp-list &
    count=0

    # Read pipe and clean out files
    while read -r line
    do
      if [[ -n "$line" ]]
      then
        rm -f "$line"
        # shellcheck disable=SC2181
        [[ $? -eq 0 ]] && [[ "${verbosity,,}" == "yes" ]] && echo "rm -f $line"
        (( count++ ))
      fi
    done < ~/pipe/tmp-list

  print_status
  done
fi


script_end "nocount/noexec/nomail" >> "$logfile" &
script_end "nocount/noexec/nomail"

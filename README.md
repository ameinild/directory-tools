# BASH Script: directory-tools

This BASH script can perform the following operations on any directories you specify:

* Change owner and permissions recursively
* Touch symlink, so the symlink date equals the date of the target file
* Clean out .tmp, .bak and .swp files

The script is intended for running daily with cron to automate the above mentioned tasks.

The script validates with `shellcheck -x`.

## Preliminary information

* This script must be run as a root user
* This script requires the support script `bash-common.sh` to be placed (or linked) in the same directory as the script
* This script will create 2 named pipes: `~/pipe/sym-list` and `~/pipe/tmp-list`

## The configuration file

This script comes with a default configuration file: `directory-tools.conf`

The configuration file allows for the following general options:

* `logfile=` : Sets the file where formatted output is logged (if set to an invalid path, logfile is set to `/dev/null`)
* `user=` : Sets the default user (mainly used for testing)
* `verbosity=` : Yes/No. Indicates if the script echoes the commands in addition to performing them

In addition, the configuration file contains the following 3 arrays, where directories (and owner/permissions) are specified:

* `roppaths=` : Array with a 3 part string (path owner permissions) for changing owner and permissions
* `sympaths=` : Array with a single path string for touching symlinks
* `tmppaths=` : Array with a single path string for cleaning tmpfiles

The default configuration file contains examples of these arrays.

## The script file

The main script file `directory-tools.sh` also needs to be adjusted a bit before use.

* Line 10: Set the path where the script and configuration files are executed from
* Line 23: Set the same path (for shellcheck validation only)
* Line 27-29: Can be used to override config paths while testing

## Running the script

When all parameters are set correctly, the script can be run with `./directory-tools.sh`.

The recommended method of execution is putting the script in your chosen destination, and making a symlink in `/etc/cron.daily` to the script. Note that the symlink should be named `directory-tools` without extension.
